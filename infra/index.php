<!DOCTYPE html>
<html lang="fr">
  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Infrastructure - Matthieu ASLAN</title>
      <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" /> <!--aos css-->
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
      <!--Google Odibee Sans Cookie Start 2p font-->
      <link rel="preconnect" href="https://fonts.gstatic.com">
      <link href="https://fonts.googleapis.com/css2?family=Cookie&display=swap" rel="stylesheet"> 
      <link href="https://fonts.googleapis.com/css2?family=Odibee+Sans&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="main.css">
  </head>
  <body>
      <header>
        <div class="menu-toggler" id="top">
          <div class="bar half start"></div>
          <div class="bar"></div>
          <div class="bar half end"></div>
        </div>
          <h1>Infrastructure - Presentation</h1>
          <nav class="top-nav">
            <ul class="nav-list">
              <li>
                <a href="../index.php" class="nav-link" onClick="history.go(0)"  >Acceuil</a>
              </li>
              <li>
                <a href="index.php#showcase" class="nav-link" onClick="history.go(0)"  >Presentation</a>
              </li>
              <li>
                  <a href="index.php#works"  class="nav-link" onClick="history.go(0)"  >Travaux</a>
                </li>
              <li>
                  <a href="../cybersecu/index.php" class="nav-link" onClick="history.go(0)"  >Cyber Sécurité</a>
                </li>
            </ul>
          </nav>
      </header>
      <div class="presentation" id="pres">
        <a href="https://fr.wikipedia.org/wiki/R%C3%A9seau_informatique" target='_blank'><h2>Réseau informatique</h2></a>
        <p>Un réseau informatique (en anglais, data communication network ou DCN) est un ensemble d'équipements reliés entre eux pour échanger des informations. Par analogie avec un filet (un réseau est un « petit rets », c'est-à-dire un petit filet), on appelle nœud l'extrémité d'une connexion, qui peut être une intersection de plusieurs connexions ou équipements (un ordinateur, un routeur, un concentrateur, un commutateur).Indépendamment de la technologie sous-jacente, on porte généralement une vue matricielle sur ce qu'est un réseau.De façon horizontale, un réseau est une strate de trois couches : les infrastructures, les fonctions de contrôle et de commande, les services rendus à l'utilisateur. De façon verticale, on utilise souvent un découpage géographique : réseau local, réseau d'accès et réseau d'interconnexion. </p>
      </div>
      <div id="works" class="works">
          <?php
          include 'feed.php';
          ?>
      </div>
    <footer class="copyright">
      <a href="index.php#top">
        <div class="up" id="up">
          <!--Bouton pour remonté-->
            <i class="fas fa-chevron-up"></i>
        </div>
      </a>
          <!--Le copyright :)-->
      <p>&copy; 2020 ASLAN Matthieu</p>
    </footer>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>    
    <script src="main.js"></script>
  </body>
</html>