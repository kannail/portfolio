$(document).ready(function () {
    // Handler pour l'appel de .ready()
    $('.menu-toggler').on('click', function () {
        //Action 
        $(this).toggleClass('open');
        $('.top-nav').toggleClass('open');
    });
    $('.nav-link').on('click', function (){
        //Action 
        $(this).toggleClass('close');
        $('.top-nav').toggleClass('close')
    });
});