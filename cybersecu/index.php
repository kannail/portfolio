<!DOCTYPE html>
<html lang="fr">
  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Cyber-Sécurité - Matthieu ASLAN</title>
      <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" /> <!--aos css-->
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
      <!--Google Odibee Sans Cookie Start 2p font-->
      <link rel="preconnect" href="https://fonts.gstatic.com">
      <link href="https://fonts.googleapis.com/css2?family=Cookie&display=swap" rel="stylesheet"> 
      <link href="https://fonts.googleapis.com/css2?family=Odibee+Sans&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="main.css">
  </head>
    <body>
      <header>
        <div class="menu-toggler" id="top">
          <div class="bar half start"></div>
          <div class="bar"></div>
          <div class="bar half end"></div>
        </div>
        <h1>Cyber-Sécurité - Presentation</h1>
        <nav class="top-nav">
          <ul class="nav-list">
            <li>
              <a href="../index.php" class="nav-link" onClick="history.go(0)"  >Acceuil</a>
            </li>
            <li>
              <a href="index.php#pres" class="nav-link" onClick="history.go(0)"  >Presentation</a>
            </li>
            <li>
              <a href="index.php#works" class="nav-link" onClick="history.go(0)"  >Travaux</a>
            </li>
            <li>
              <a href="../infra/index.php" class="nav-link" onClick="history.go(0)"  >Infrastructure</a>
            </li>
          </ul>
        </nav>
      </header>
      <div class="presentation" id="pres">
        <a href="https://fr.wikipedia.org/wiki/Cybers%C3%A9curit%C3%A9" target='_blank'><h2>Concept général de la cybersécurité</h2></a>
        <p>La cybersécurité, qui concerne la sécurité et la souveraineté numérique de chaque État-Nation, présente des enjeux économiques, stratégiques et politiques qui vont donc bien au-delà de la seule Sécurité des systèmes d'information. Elle concerne d'ailleurs aussi bien l'informatique de gestion, l'informatique industrielle, l'informatique embarquée que les objets connectés. La cybersécurité doit être appréhendée de manière holistique pour prendre en compte les aspects économiques, sociaux, éducatifs, juridiques, techniques, diplomatiques, militaires et de renseignement3. Excellence technique, adaptabilité et coopération sont essentielles dans ce domaine.</p>
      </div>
      <div id="works" class="works">
          <?php
          include 'feed.php';
          ?>
      </div>
      <footer class="copyright">
        <a href="index.php#top">
          <div class="up" id="up">

          <!--Bouton pour remonté-->
            <i class="fas fa-chevron-up"></i>
          </div>
        </a>
          <!--Le copyright :)-->
        <p>&copy; 2020 ASLAN Matthieu</p>
      </footer>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://unpkg.com/aos@next/dist/aos.js"></script>    
      <script src="main.js"></script>
    </body>
</html>