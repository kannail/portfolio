<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Matthieu ASLAN</title>
        <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <!--Google font-->
        <link href="https://fonts.googleapis.com/css2?family=Cookie&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Press+Start+2P&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Odibee+Sans&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="main.css">
    </head>
    <body>
        <header>
            <!--Menu burger-->
            <div class="menu-toggler">
                <div class="bar half start"></div>
                <div class="bar"></div>
                <div class="bar half end"></div>
            </div>
            <nav class="top-nav">
            <!--Navigation-->
                <ul class="nav-list">
                    <li>
                        <a href="index.php" class="nav-link">Home</a>
                    </li>
                    <li>
                        <a href="#about" class="nav-link" onClick="history.go(0)" >About</a>
                    </li>
                    <li>
                        <a href="#services" class="nav-link" onClick="history.go(0)">Services</a>
                    </li>
                    <li>
                        <a href="#portfolio" class="nav-link" onClick="history.go(0)">Portfolio</a>
                    </li>
                    <li>
                        <a href="#experiences" class="nav-link" onClick="history.go(0)">Experiences</a>
                    </li>
                    <li>
                        <a href="#contact" class="nav-link" onClick="history.go(0)">Contact</a>
                    </li>
                    <li>
                        <p style="font-family: 'Cookie', cursive">Expérience detaillé et mis a jour</p>
                    </li>
                    <li>
                        <a href="cybersecu/index.php" class="nav-link" onClick="history.go(0)" >Cyber Sécurité</a>
                    </li>
                    <li>
                        <a href="infra/index.php" class="nav-link" onClick="history.go(0)">Infrastructure</a>
                    </li>
                </ul>
            </nav>

            <!--wow its me-->
            <div class="landing-text">
                <h1>Matthieu ASLAN</h1>
                <h6>Technicien télécoms et réseaux</h6>
            </div>
        </header>

        <section class="about" id="about">
            <div class="container">
                <div class="profile-img">
                    <img src="images/profil.jpg" alt="">
                </div>

            <!--Description de moi et de ce que j'aimerais realisé-->
                <div class="about-details">
                    <div class="about-heading">
                        <h2>Qui</h2>
                        <h6>suis-je</h6>
                    </div>
                    <p>
                        Je suis un étudiant de terminale bac professionnel Système numérique. Depuis mon plus jeune âge je
                        suis passionné de technologie et commencer à manipuler les ordinateurs très tôt mon frère ayant 7
                        ans de plus que moi à beaucoup contribuer a mon apprentissage rapide de l’informatique. Ma formation
                        a aussi étais une aide majeure qui ma permis de tester mes capacité et d’en apprendre de plus en
                        plus A la fin de cette année j’aimerais intégrer l'école de campus académie qui grâce à leur
                        programme pourront m’aider à apprendre de nouvelle chose de A a Z ce qui me pousserait à continuer
                        les études jusqu’au master 2 et pourquoi pas passer un doctorat.
                    </p>
            <!--Reseau Sociaux-->
                    <div class="social-media">
                        <ul class="nav-liste">
                            <li>
                                <a href="#" class="icon-link">
                                    <i class="fab fa-facebook-square"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="icon-link">
                                    <i class="fab fa-twitter-square"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="icon-link">
                                    <i class="fab fa-discord"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
            <!--Les different poste que j'ai deja eu-->

        <section class="services" id="services">
            <div class="container">
                <div class="section-heading">
                    <h2>Services</h2>
                    <h6>Que puis-je faire</h6>
                </div>
                <div class="my-skills">
                    <div class="skill">
                        <div class="icon-container">
                            <i class="fas fa-cogs"></i>
                        </div>
                        <h2>Technicien de maintenance en informatique</h2>
                        <p>
                            Un virus, des fichiers volatilisés, un écran noir, une imprimante bloquée...
                            Tel est le lot quotidien du technicien de maintenance en informatique.
                            Véritable urgentiste, il veille au bon fonctionnement des matériels comme des logiciels
                        </p>
                    </div>
                    <div class="skill">
                        <div class="icon-container">
                            <i class="fas fa-broadcast-tower"></i>
                        </div>
                        <h2>Technicien télécoms et réseaux</h2>
                        <p>
                            Son domaine : les liaisons (avec ou sans fil) des équipements téléphoniques et informatiques.
                            Que la transmission soit opérée par câble, fibre optique, satellite ou voie hertzienne.
                            Trois grands axes d'activité pour ce professionnel : l'installation, la maintenance
                            et le conseil.
                        </p>
                    </div>
                    <div class="skill">
                        <div class="icon-container">
                            <i class="fas fa-code-branch"></i>
                        </div>
                        <h2>Installateur en télécoms</h2>
                        <p>
                            L'installateur en télécoms installe les équipements et réseaux télécoms des particuliers,
                            des entreprises et des administrations (téléphone, échange de messages textes, images et son).
                            Puis il s'assure de leur bon fonctionnement.
                        </p>
                    </div>
                </div>
            </div>
        </section>
            <!--Mes domaine d'étude-->

        <section class="portfolio" id="portfolio">
            <div class="container">
                <div class="section-heading">
                    <h2>Portfolio</h2>
                    <h6>Mes traveaux personel recent</h6>
                </div>
                <div class="portfolio-item">
                    <div class="portfolio-img has-margin-right">
                        <img src="images/cybersecu.jpg" alt="">
                    </div>
                    <div class="portfolio-description has-margin-right">
                        <h6>Cyber-Sécurité</h6>
                        <h2>Description</h2>
                        <p>
                            Ensemble des demarche qui peuvent être utilisés pour protéger les personnes et les actifs informatiques matériels et immatériels (connectés directement ou indirectement à un réseau) des états et des organisations (avec un objectif de disponibilité, intégrité & authenticité, confidentialité, preuve & non-répudiation).                        
                        </p>
                        <a href="cybersecu/index.php" class="cta">View Details</a>
                    </div>
                </div>
                <div class="portfolio-item">
                    <div class="portfolio-description">
                        <h6>Programation</h6>
                        <h2>GitLab</h2>
                        <p>
                            GitLab is a global company that provides hosting for software development version control
                            using Git. 
                            It offers all of the distributed version control and source code management functionality of Git
                            as well as adding its own features.
                        </p>
                        <a href="https://gitlab.com/kannail/" class="cta" target="_blank">View Details</a>
                    </div>
                    <div class="portfolio-img">
                        <img src="images/portitem2.png" alt="">
                    </div>
                </div>
                <div class="portfolio-item">
                    <div class="portfolio-img has-margin-right">
                        <img src="images/infra.jpg" alt="">
                    </div>
                    <div class="portfolio-description">
                        <h6>Infrastructure</h6>
                        <h2>Virtualisation</h2>
                        <p>
                            Virtualization is the process of running a virtual instance of a computer system in a layer
                            abstracted from the actual hardware. Most commonly, it refers to running multiple operating
                            systems on a computer system simultaneously.
                        </p>
                        <a href="infra/index.php" class="cta">View Details</a>
                    </div>
                </div>
            </div>
        </section>
            <!--Mes experience pro-->

        <section class="experiences" id="experiences">
            <div class="container">
                <div class="section-heading">
                    <h2>Work Experiences</h2>
                    <h6>Ou ai-je travailler</h6>
                </div>
                <div class="timeline">
                    <ul>
            <!--include du "feed.php" ou generé mes exp pro-->
                        <?php include ('feed.php');?>
                    </ul>
                </div>
            </div>
        </section>
            <!--Formulaire de contact-->

        <section class="contact" id="contact">
            <div class="container">
                <div class="section-heading">
                    <h2>Contact</h2>
                    <h6>Let's enjoy our work</h6>
                </div>
                <form action="formulaire.php" method="post">
                    <label for="name">Name:</label>
                    <input type="text" id="name" name="name" placeholder="Enter your name..." required>
                    <label for="email">E-mail:</label>
                    <input type="email" id="email" name="email" placeholder="Enter your email..." required>
                    <label for="subject">Subject:</label>
                    <textarea name="subject" id="subject" cols="10" rows="10"></textarea>
                    <input type="submit" value="Submit">
                </form>
            </div>
        </section>
        <footer class="copyright">

        <a href="index.php#about">
            <div class="up" id="up">
            <!--Bouton pour remonté-->
                <i class="fas fa-chevron-up"></i>
            </div>
        </a>
            <!--Le copyright :)-->
        <p>&copy; 2020 ASLAN Matthieu</p>
        </footer>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
        <script src="main.js"></script>
    </body>
</html>