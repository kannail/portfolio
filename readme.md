# My portfolio

## Presentation global

## Attention

Les fichier php ne passe pas directement au w3c valisator, il faut copier le fichie et le mettre en direct input
```
https://validator.w3.org/#validate_by_input
```

### Besoin

J'avais besoin dans certain cas de pouvoir ajouter des information facilement selon un template, je suis donc passer par ce que j'ai analyser comme etant un modéle vue controleur.

La vue etant l'index, elle va afficher les information du controleur, soit le "feed" qui va recupéré ses information sur model.

Ce qui me permet par la suite avec un fichier php qui formatte bien mon html d'obtenir un code propre et auto générer en sortie.

Parce que j'ai trouver comment faire, j'ai ajouter un petit fichier javascript coupler a mon css pour obtenir mon menu burger avoir une animation au clique et d'arrivé du menu.

Le text vient de mon ancien portfolio, je me suis plus focaliser sur le coté technique, et compte le remplir par la suite.

### "Algo" du script php
Je trouvais le code beaucoup trop lourd pour ce que c'est avec tout les commentaire, j'ai donc laisser les script feed.php de la cyber secu et de l'infra sans commentaire

 * Récuperation du fichier json
 * "decode" du fichier json
 * Initialisation de la variable de boucle et d'une boucle do while
 * On commence par recupérer le contenue du poste a afficher
 * On genere un html adapter comprenant nos variable
 * On incremente i(notre variable de boucle) et on fait ca le nombre de fois qu'il y a d'objet count($obj)
 
### main.js explication

```javascript
$(document).ready(function () {
    // Handler pour l'appel de .ready() .
    $('.menu-toggler').on('click', function () {
        //Action 
        $(this).toggleClass('open');
        $('.top-nav').toggleClass('open');
    });
    $('.nav-link').on('click', function (){
        //Action
        $(this).toggleClass('close');
        $('.top-nav').toggleClass('close')
    });
});
```
### Récupération du formulaire dans un fichier contact.json(pas des plus util, la lecture n'est pas facile est rien n'est sécurisé)

 * Recuperation du formulaire dans un tableau
 * Encode tableau vers json
 * Convertion en json
 * Ecriture dans le fichier
 * Redirection

### Accesibilité a mes document

cybersecu : Me permet de publié mes recherche en cybersécu
infra : Me permet de publié mes recherche en reseau
main : Me permet de me presenté et de publié tout mes rapport de stage 

### Explication erreur w3c
Les seul erreur sur les fichier php vien de l'appele de fontion php.


## Perso, mais cela peut etre interessant pour comprendre

### Architercture :

```
- projet/:
- contact .json (Récupère lse information du formulaire)
- formulaire.php (sauvgarde les données du formulaire dans l'index dans contact .json)
- exppro.json (Stockage des experience profesionnel)
- index.php (Affiche un résumé de qui je suis et de pouquoi je fait ce que je fait)
- feed.php (utilisation de "exppro.json" pour envoyer les exp profesionnel a l'index)
- main.css (feuille de style pour l'index principal)
- main.js (Sert a faire fonctionné le menu)
- readme.md (Sert a commprendre se projet :)
- projet/rapport/:
- *.pdf (Stockage de tout mes rapport de stage)
- projet/cybersecu/:
- work.json (Servira a englobé les different projet en cybersécurité, et a terme joindre des doc en pdf avec des poc et des documentation)
- index.php (Presente ma vue de la cyber sécu et affiche les different projet)
- feed.php (meme mecanique qu'au dessus, je recupere mon point json et envoie l'html formatter comme je le desire pour mon site)
- main.css (feuille de style pour l'index de cyber sécu)
- main.js (detect le clique sur le menu pour l'ouvrir)
- projet/cybersecu/post:
- *.png     {
- *.jpg     Concerve les photo et documentation des travaux en cyber sécu
- *.pdf     }
- projet/infra/:
- work.json
- index.php
- feed.php
- main.css
- main.js
- projet/infra/post:
- *.png
- *jpg
- *.pdf
```

### WorkFlow

```
- projet/:
- contact .json {indentation}21/mai 10h45
- formulaire.php {indentation}/mai 10h52
- exppro.json {indentation}21/mai 10h45
- index.php {w3c check + indentation}21/mai 10h50
- feed.php {indentation ok 21/main 10h46}
- main.css {indentation}21/mai 10h45
- main.js {indentation}21/mai 10h45
- readme.md
- projet/cybersecu/:
- work.json {indentation}21/mai 10h44
- index.php{w3c + indentation}21/mai 10h40
- feed.php {indentation}21/mai 10h35
- main.css {w3c + identation}21/mai 10h42
- main.js {indentation}21/mai 10h44
- projet/infra/:
- work.json {indentation}21/mai 10h33
- index.php {w3c check+ indentation}21/mai 10h15
- feed.php {indentation}21/mai 10h00
- main.css {w3c check+ indentation}21/mai 10h30
- main.js {indentation}21/mai 10h32
```

## GitLab Connexion

```
Command line instructions

You can also upload existing files from your computer using the instructions below.
Git global setup

git config --global user.name "ASLAN"
git config --global user.email "aslanmatthieu@gmail.com"

Create a new repository

git clone git@gitlab.com:kannail/portfolio.git
cd portfolio
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Push an existing folder

cd existing_folder
git init
git remote add origin git@gitlab.com:kannail/portfolio.git
git add .
git commit -m "Initial commit"
git push -u origin master

Push an existing Git repository

cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:kannail/portfolio.git
git push -u origin --all
git push -u origin --tags

```

