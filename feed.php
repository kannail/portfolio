<?php
/**
 * Récuperation du fichier json
 */

    $file = 'exppro.json';
    $data = file_get_contents($file);
/**
 * "decode" du fichier json
 */
    $obj = json_decode($data);  

/**
 * Initialisation de la variable de boucle et d'une boucle do while 
 */
    $i = 0;
    do{


/**
 * On commence par recupérer le contenue du poste a afficher
 */

        $date=$obj[$i]->date;
        $rapport=$obj[$i]->rapport;
        $title=$obj[$i]->titre;
        $fonction=$obj[$i]->fonction;
        $contenue=$obj[$i]->contenue;


/**
 * On genere un html adapter comprenant nos variable
 */

        echo "<li class='date' data-date='".$date."'>";
        echo "<a href = ".$rapport." target='_blank'>";
        echo "<h1>".$title."</h1></a>";
        echo "<h6>".$fonction."</h6>";
        echo "<p>".$contenue."</p>";
        echo "</li>";


/**
 * On incremente i(notre variable de boucle) et on fait ca le nombre de fois qu'il y a d'objet count($obj)
 */
        $i = $i + 1;
    }while($i != count($obj));

?>